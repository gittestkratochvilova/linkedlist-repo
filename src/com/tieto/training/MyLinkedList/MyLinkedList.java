package com.tieto.training.MyLinkedList;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class MyLinkedList<E> implements List<E> {

	private Node<E> head;
	private Node<E> tail;
	private int size;

	LastCall lastedCalled;

	private enum LastCall {
		NEXT, PREVIOUS, NOTHING;
	}

	public MyLinkedList() {
		clear();
		lastedCalled = LastCall.NOTHING;
	}

	private Node<E> findNode(int index) {
		Node<E> node = head;
		for (int i = 0; i < index; i++) {
			node = node.next;
		}
		return node;
	}

	@Override
	public boolean add(E data) {
		Node<E> node = new Node<>(data);
		if (head == null) {
			head = node;
		} else {
			tail.next = node;
			node.prev = tail;
		}
		tail = node;
		size++;

		return true;
	}

	@Override
	public void add(int index, E element) {
		checkIndex(index);
		Node<E> addNode = new Node<>(element);
		Node<E> node2 = findNode(index);
		Node<E> node1 = node2.prev;
		node1.next = addNode;
		addNode.prev = node1;
		addNode.next = node2;
		node2.prev = addNode;
	}

	@Override
	public boolean addAll(Collection<? extends E> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends E> arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		head = null;
		tail = null;
		size = 0;
	}

	@Override
	public boolean contains(Object arg0) {
		if (indexOf(arg0) == -1) {
			return false;
		}
		return true;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E get(int index) {
		checkIndex(index);

		Node<E> node = head;
		for (int i = 0; i < index; i++) {
			node = node.next;
		}

		return node.data;
	}

	private void checkIndex(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public int indexOf(Object arg0) {
		Node<E> node = head;
		for (int i = 0; i < size; i++) {
			if (Objects.equals(node.data, arg0)) {
				return i;
			}
			node = node.next;
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	class MyListIterator<E> implements ListIterator<E> {

		private Node<E> position = (Node<E>) head;
		private int index = 0;

		@Override
		public boolean hasNext() {
			return position.next == null;
		}

		@Override
		public E next() {
			if (position == null) {
				throw new NoSuchElementException();
			}

			final E element = position.data;
			position = position.next;
			index++;
			lastedCalled = LastCall.NEXT;

			return element;
		}

		@Override
		public void add(E element) {
			MyLinkedList.this.add(index, element);
			lastedCalled = LastCall.NOTHING;
		}

		@Override
		public boolean hasPrevious() {
			if (position.prev == null) {
				return false;
			}
			return true;
		}

		@Override
		public int nextIndex() {

			return index;
		}

		@Override
		public E previous() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			E data = position.data;
			position = position.prev;
			index--;
			lastedCalled = LastCall.PREVIOUS;

			return data;
		}

		@Override
		public int previousIndex() {
			return index - 1;
		}

		@Override
		public void remove() {
			if (lastedCalled == LastCall.NOTHING) {
				throw new IllegalStateException();
			}

			if (lastedCalled == LastCall.NEXT) {
				MyLinkedList.this.remove(index - 1);
			}
			if (lastedCalled == LastCall.PREVIOUS) {
				MyLinkedList.this.remove(index);
			}
			lastedCalled = LastCall.NOTHING;
			if (index == size) {
				index--;
			}
		}

		@Override
		public void set(E element) {
			if (lastedCalled == LastCall.NOTHING) {
				throw new IllegalStateException();
			}

			if (lastedCalled == LastCall.NEXT) {
				MyLinkedList.this.set(index - 1, element);
			}
			if (lastedCalled == LastCall.PREVIOUS) {
				MyLinkedList.this.set(index, element);
			}
			lastedCalled = LastCall.NOTHING;
		}

	}

	@Override
	public Iterator<E> iterator() {
		Iterator<E> iterator = new MyListIterator<>();
		return iterator;
	}

	@Override
	public int lastIndexOf(Object element) {
		Node<E> node = tail;
		for (int i = size - 1; i >= 0; i--) {

			if (Objects.equals(element, node.data)) {
				return i;
			}
			node = node.prev;
		}
		return -1;
	}

	@Override
	public ListIterator<E> listIterator() {
		ListIterator<E> listIterator = new MyListIterator<>();
		return listIterator;
	}

	@Override
	public ListIterator<E> listIterator(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object element) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E remove(int index) {
		checkIndex(index);
		Node<E> node = findNode(index);

		node.next.prev = node.prev;
		node.prev.next = node.next;

		return node.data;
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E set(int index, E element) {
		checkIndex(index);
		Node<E> node = findNode(index);
		E returned = node.data;
		node.data = element;
		return returned;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public List<E> subList(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	private static class Node<E> {
		private Node<E> prev;
		private Node<E> next;
		private E data;

		public Node(E data) {
			this.data = data;
		}

	}

}
