package com.tieto.training.linkedlist;

import static org.junit.Assert.*;

import org.junit.Test;

public class LinkedListRemoveTest extends LinkedListTest
{	

	@Test
	public void testRemove()
	{
		list.add(S02);
		list.add(S01);
		list.add(S03);
		
		assertEquals(S01, list.remove(1));		
		
	}
	
	@Test
	public void testRemoveNegativeNumbers()
	{
		list.add(S02);
		list.add(S01);
		list.add(S03);

		try
		{
			list.remove(-5);
			fail("Expected IndexOutOfBoundsExpection");
		} 
		catch (IndexOutOfBoundsException ex)
		{
		}
	}
	
	@Test
	public void testRemoveOutOfRange()
	{
		list.add(S02);
		list.add(S01);
		list.add(S03);

		try
		{
			list.remove(15);
			fail("Expected IndexOutOfBoundsExpection");
		} 
		catch (IndexOutOfBoundsException ex)
		{
		}
	}

}
