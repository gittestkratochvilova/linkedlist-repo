package com.tieto.training.linkedlist;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.tieto.training.MyLinkedList.MyLinkedList;

public class LinkedListTest
{

	protected final String S01 = "David";
	protected final String S02 = "Patrik";
	protected final String S03 = "Jakub";
	protected final String S04 = "Petr";
	protected final String S05 = "Michal";
	protected final String S06 = "Lukas";
	protected final String S07 = "Ales";
	protected final String S08 = "Filip";
	protected final String S09 = "Denis";
	protected final String S10_08 = "Filip";
	protected final String S11_03 = "Jakub";
	protected List<String> list;

	@Before
	public void setUp()
	{
		list = new MyLinkedList<>();
	}

	@Test
	public void test()
	{
		assertEquals(0, list.size());
		assertTrue(list.isEmpty());

		list.add("Hello");
		assertEquals(1, list.size());
	}

	@Test
	public void testAdd()
	{
		final int listSize = 10000;
		for (int i = 0; i < listSize; i++)
		{
			list.add(String.valueOf(i));
		}

		assertEquals(listSize, list.size());
	}

	@Test
	public void testClearEmptyList()
	{
		assertEquals(0, list.size());
		list.clear();
		assertEquals(0, list.size());
	}

	@Test
	public void testClearList()
	{
		list.add(S01);
		list.add(S02);
		list.clear();
		assertEquals(0, list.size());
	}

	@Test
	public void testGet()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		assertEquals(S02, list.get(1));
	}
	
	@Test
	public void testGetFirst()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		assertEquals(S01, list.get(0));
	}
	
	@Test
	public void testGetLast()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		assertEquals(S03, list.get(2));
	}
	
	@Test
	public void testGetJustBeforeZero()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		try
		{			
			list.get(-1);
			fail("Expected IndexOutOfBoundsException");
			
		}
		catch (IndexOutOfBoundsException ex)
		{
			
		}
	}

	@Test
	public void testGetBeforeZero()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		try
		{
			list.get(-11);
			fail("Expected IndexOutOfBoundsExpection");
		} 
		catch (IndexOutOfBoundsException ex)
		{
		}
	}
	
	@Test
	public void testGetJustAfterLast()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		try
		{			
			list.get(3);
			fail("Expected IndexOutOfBoundsException");
			
		}
		catch (IndexOutOfBoundsException ex)
		{			
		}
	}

	@Test
	public void testGetAfterLast()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		try
		{
			list.get(15);
			fail("Expected IndexOutOfBoundsExpection");
		} 
		catch (IndexOutOfBoundsException ex)
		{
		}
	}
	
	@Test
	public void testContains()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		assertTrue(list.contains(S02));
	}
	
	@Test
	public void testNotContains()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		assertFalse(list.contains(S05));
	}
	
	@Test
	public void testIndexOf()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		assertEquals(1, list.indexOf(S02));
	}
	
	@Test
	public void testIndexOfNotExistable()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);

		assertEquals(-1, list.indexOf(S05));
	}	
	
	@Test
	public void testSet()
	{
		list.add(S01);
		list.add(S02);
		list.add(S03);
		
		list.set(1, S04);

		assertEquals(S04, list.get(1));
	}
	
	@Test
	public void testSetNegativeNumber()
	{
		list.add(S02);
		list.add(S01);
		list.add(S03);

		try
		{
			list.set(15, S05);
			fail("Expected IndexOutOfBoundsExpection");
		} 
		catch (IndexOutOfBoundsException ex)
		{
		}
	}
	
	@Test
	public void testSetOutOfRange()
	{
		list.add(S02);
		list.add(S01);
		list.add(S03);

		try
		{
			list.set(15, S05);
			fail("Expected IndexOutOfBoundsExpection");
		} 
		catch (IndexOutOfBoundsException ex)
		{
		}
	}	

}
